(function() {
  angular
  .module('studiolo.keyboard')
  .controller('KeyboardController', KeyboardController);

  KeyboardController.$inject = ['$scope','$rootScope','$location','hotkeys','keyboardFactory','fileFactory']

  function KeyboardController($scope,$rootScope,$location,hotkeys,keyboardFactory,fileFactory) {
    $scope.keyboardModel = keyboardFactory.keyboardModel;
    $scope.activeKeyGroup = 0;
    $scope.activeKeyIndex = 0;
    $scope.input = '';
    $scope.process = null;

    $scope.saveFile = saveFile;

    $scope.keyUp = keyUp;
    $scope.keyDown = keyDown;
    $scope.keyLeft = keyLeft;
    $scope.keyRight = keyRight;
    $scope.keyType = keyType;

    function saveFile(){
      console.log('saving file');
      fileFactory.fname = $scope.input;
      fileFactory.status = 'SAVING';
      console.log($rootScope.referer);
      $location.path('/camera');
    }

    function keyUp(){
      var nextIndexValue = $scope.activeKeyIndex - 9;
      if(nextIndexValue>=0){
        $scope.activeKeyIndex= nextIndexValue;
      }else{
        if($scope.activeKeyGroup>0){
          $scope.activeKeyGroup--;
          $scope.activeKeyIndex=0;
        }

      }
    }

    function keyDown(){
      var nextIndexValue = $scope.activeKeyIndex + 9;
      if(nextIndexValue<$scope.keyboardModel.keys_groups[$scope.activeKeyGroup].keys.length-1){
        $scope.activeKeyIndex= nextIndexValue;
      }else{
        if($scope.activeKeyGroup<$scope.keyboardModel.keys_groups.length-1){
          $scope.activeKeyGroup++;
          $scope.activeKeyIndex=0;
        }

      }
    }

    function keyLeft(){
      if($scope.activeKeyIndex>0)
        $scope.activeKeyIndex--
    }

    function keyRight(){
      if($scope.activeKeyIndex<$scope.keyboardModel.keys_groups[$scope.activeKeyGroup].keys.length-1)
        $scope.activeKeyIndex++
    }

    function keyType(){
        var group_type = $scope.keyboardModel.keys_groups[$scope.activeKeyGroup].group_type;
        var key = $scope.keyboardModel.keys_groups[$scope.activeKeyGroup].keys[$scope.activeKeyIndex];
        if(group_type == 'button') {
          switch (key.type) {
            case 'link':
              if(key.value=='referer')
                $location.path($rootScope.referer);
              else {
                $location.path(key.value);
              }
              break;
            case 'callback':
              $scope[key.value]();
              break;
            case 'command':
              console.log('command');
              break;

            default:

          }
        }else{
      if(key=='del') {
      $scope.input = $scope.input.slice(0,-1);
      }else{
      $scope.input += key;
    }
        }
    }

    hotkeys.bindTo($scope)
    .add({
      combo: 'down',
      description: 'Down',
      callback: keyDown
    })
    .add ({
      combo : "up",
      description : "Up",
      callback : keyUp
    })
    .add ({
      combo : "right",
      description : "Right",
      callback : keyRight
    })
    .add ({
      combo : "left",
      description : "Left",
      callback : keyLeft
    })
    .add ({
      combo : "a",
      description : "Type",
      callback : keyType
    });

  }
})();
