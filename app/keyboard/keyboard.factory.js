(function() {
  angular
    .module('studiolo')
    .factory('keyboardFactory', keyboardFactory);

  function keyboardFactory() {
      return {
        keyboardModel :{
          keys_groups : [
            {
              group_type : 'key',
              keys : ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','del']
            },
            {
              group_type : 'button',
              keys : [
                { title : 'Enregistrer', type : 'callback', value : 'saveFile'},
                { title : 'Annuler', type : 'link', value : 'referer'}
              ]
            }
          ]
        }

     };
  }
})();
