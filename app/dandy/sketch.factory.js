(function() {
    'use strict';

    angular
        .module('studiolo.dandy')
        .factory('scene', scene);

    scene.$inject = ['$window','p5'];

    /* @ngInject */
    function scene($window,p5) {
      return function(p) {
        var r = p.random(0, 255);
        var g = p.random(0, 255);

        var segLength = 80,
          x, y, x2, y2;

          function dragSegment(i, xin, yin) {
            p.background(0);

            var dx = p.mouseX - x;
            var dy = p.mouseY - y;
            var angle1 = p.atan2(dy, dx);

            var tx = p.mouseX - p.cos(angle1) * segLength;
            var ty = p.mouseY - p.sin(angle1) * segLength;
            dx = tx - x2;
            dy = ty - y2;
            var angle2 = p.atan2(dy, dx);
            x = x2 + p.cos(angle2) * segLength;
            y = y2 + p.sin(angle2) * segLength;

            segment(x, y, angle1);
            segment(x2, y2, angle2);
          }

          function segment(x, y, a) {
            p.push();
            p.translate(x, y);
            p.rotate(a);
            p.line(0, 0, segLength, 0);
            p.pop();
          }


        p.setup = function() {
          p.createCanvas(angular.element($window).width(), angular.element($window).height());
          p.strokeWeight(20);
          p.stroke(255, 100);

          x = p.width/2;
          y = p.height/2;
          x2 = x;
          y2 = y;
        };

        p.draw = function() {
          p.background(0);
          dragSegment(0, p.mouseX, p.mouseY);
          for( var i=0; i<x.length-1; i++) {
            dragSegment(i+1, x[i], y[i]);
          }
        };
      };
    }
})();
