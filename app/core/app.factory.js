(function() {
  angular
    .module('studiolo')
    .factory('appFactory', appFactory);

  function appFactory() {
      return {
        appModel :
          { camera :
            {
              app_title : "Camera",
              app_icon : "camera",
              app_back_entry : {
                item_slug : "back",
                item_title : "Retour",
                item_action : null,
                item_description : "Revenir en arrière",
                item_path : null
              },
              app_items : [
                {
                  item_slug : "capture",
                  item_title : "Capture",
                  item_action : null,
                  item_description : "Prendre des photos ou enregistrer une video",
                  item_path : "/"
                },
                {
                  item_slug : "watch",
                  item_title : "Visionne",
                  item_action : {
                    type : "callback",
                    name : "do_watch",
                    value : "viewer"
                  },
                  item_description : "Regarde tes images et tes vidéos",
                  item_path : "/"
                },
                {
                  item_slug : "video",
                  item_title : "Vidéo",
                  item_action : {
                    type : "callback",
                    name : "do_video",
                    value : "preview"
                  },
                  item_description : "Enregistre une video",
                  item_path : "/capture/"
                },
                {
                  item_slug : "photo",
                  item_title : "Photo",
                  item_action :  {
                    type : "command",
                    name : "do_photo",
                    value : "preview"
                  },
                  item_description : "Prend une photo",
                  item_path : "/capture/"
                },
                {
                  item_slug : "quit",
                  item_title : "Quitter",
                  item_action :  {
                    type : "link",
                    name : "do_quit",
                    value : "/"
                  },
                  item_description : "Quitter l'application",
                  item_path : "/"
                },
                {
                  item_slug : "start_video",
                  item_title : "Démarrer",
                  item_action : {
                    type : "callback",
                    name : "do_start_video",
                    value : "start_video"
                  },
                  item_description : "Lance l'enregistrement de la video",
                  item_path : "/capture/video/"
                },
                {
                  item_slug : "stop_video",
                  item_title : "Arreter",
                  item_action : {
                    type : "callback",
                    name : "do_stop_video",
                    value : "stop_video"
                  },
                  item_description : "Arrete l'enregistrement de la video",
                  item_path : "/capture/video/start_video/"
                },
                {
                  item_slug : "pause_video",
                  item_title : "Pause",
                  item_action : {
                    type : "callback",
                    name : "do_pause_video",
                    value : "pause_video"
                  },
                  item_description : "Met en pause l'enregistrement de la video",
                  item_path : "/capture/video/start_video/"
                },
              ]

            }
          }


     };
  }
})();
