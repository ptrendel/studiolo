(function() {
  angular
    .module('studiolo')
    .factory('mediaFactory', mediaFactory);

  mediaFactory.$inject = ['$rootScope'];

  function mediaFactory($rootScope) {
  var mediafactoryObj = {};

  //mediafactoryObj.path = '/home/pi/A3/studiolo/medias/videos/';
  mediafactoryObj.path = 'd:/00_WORK/A3/03_PROTO/studiolo/medias/';
  mediaFactory.modes = [
    {
      mode : 'video',
      ext : '.mp4',
      thumbnail_ext : '.jpg',
    },
    {
      mode : 'photo',
      ext : '.jpg',
      thumbnail_ext : '.jpg'
    },
    {
      mode : 'audio',
      ext : '.mp3',
      thumbnail_ext : '.jpg'
    },
    {
      mode : '3D',
      ext : '.stl',
      thumbnail_ext : '.jpg'
    }
  ]
  mediafactoryObj.loadLibrary = loadLibrary;
  mediafactoryObj.getFiles = getFiles;
  mediafactoryObj.setFile = setFile;
  mediafactoryObj.getFile = getFile;
  mediafactoryObj.files = [];
  mediafactoryObj.file = {};

  function loadLibrary(){
    mediafactoryObj.files = [];
    angular.forEach(mediaFactory.modes, function(modeObj, index) {
      console.log(modeObj.mode);
      var items = $rootScope.fs.readdirSync(mediafactoryObj.path+modeObj.mode+'/');

      for (var i=0; i<items.length; i++) {

        if(items[i].indexOf(modeObj.ext)>0) {
          var file= {};
          file.mode = modeObj.mode;
          file.filename = items[i];
          file.name = items[i].replace(modeObj.ext,'');
          file.thumb = items[i].replace(modeObj.ext,modeObj.thumbnail_ext);

          var thumb_path = mediafactoryObj.path+modeObj.mode+'/'+file.thumb;
          console.log(thumb_path);
          var isFile=true;
          try {
              stats = $rootScope.fs.statSync(thumb_path);
          }
          catch (e) {
              file.thumb = '_default_thumb.jpg';
              isFile = false;
          }
          if (isFile) {
            file.thumb = items[i].replace(modeObj.ext,modeObj.thumbnail_ext);
          }
          mediafactoryObj.files.push(file);
        }
      }
    });
    return mediafactoryObj.files;
  }

  function getFiles(){
    mediaFactory.path += 'videos/'
    var items = $rootScope.fs.readdirSync(mediafactoryObj.path);
    mediafactoryObj.files = [];
    for (var i=0; i<items.length; i++) {

      if(items[i].indexOf('.mp4')>0) {
        var file= {};
        file.filename = items[i];
        file.name = items[i].replace('.mp4','');
        file.thumb = items[i].replace('.mp4','.jpg')
        mediafactoryObj.files.push(file);
      }
    }

    return mediafactoryObj.files;
  }

  function setFile(f) {
    mediafactoryObj.file = f;
  }

  function getFile(f) {
    return mediafactoryObj.file;
  }

  return mediafactoryObj;
  }
})();
