(function() {
    'use strict';

    angular
        .module('underscorejs')
        .factory('_', _);

    _.$inject = ['$window'];

    /* @ngInject */
    function _($window) {
        return $window._;
    }
})();
