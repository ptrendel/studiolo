(function() {
    'use strict';

    angular
    .module('studiolo.launcher')
    .controller('LauncherController', LauncherController);

    LauncherController.$inject = ['$scope','$location','hotkeys','applicationsFactory']

    function LauncherController($scope,$location,hotkeys,applicationsFactory) {
      $scope.element = null;
      $scope.activeAppIndex =0;
      $scope.apps = applicationsFactory.apps;

      $scope.nextApp = nextApp;
      $scope.previousApp = previousApp;
      $scope.launchApp = launchApp;

      function nextApp(){
        if($scope.activeAppIndex < $scope.apps.length-1 ) $scope.activeAppIndex++;
      }

      function previousApp(){
        if($scope.activeAppIndex > 0 ) $scope.activeAppIndex--;
      }

      function launchApp(){
        $location.path($scope.apps[$scope.activeAppIndex].app_path);
      }

      hotkeys.bindTo($scope)
      .add({
        combo: 'right',
        description: 'Next App',
        callback: nextApp
      })
      .add ({
        combo : "left",
        description : "Previous App",
        callback : previousApp
      })
      .add ({
        combo : "a",
        description : "Launch App",
        callback : launchApp
      });


    }
})();
