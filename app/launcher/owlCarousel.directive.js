(function() {
    'use strict';

    angular
      .module('studiolo.launcher')
      .directive('owlCarousel', owlCarousel)
      .directive('owlCarouselItem',owlCarouselItem);

    function owlCarousel(){
      return {
          restrict: 'E',
          transclude: false,
          link: function (scope) {
              scope.initCarousel = function(element) {
                // provide any default options you want
                  var defaultOptions = {
                    afterInit:function(){
                        $(".owl-item").eq(scope.activeAppIndex).addClass('selected');
                    }
                  };
                  var customOptions = scope.$eval($(element).attr('data-options'));
                  // combine the two options objects
                  for(var key in customOptions) {
                      defaultOptions[key] = customOptions[key];
                  }
                  // init carousel
                  scope.element = $(element);
                  $(element).owlCarousel(defaultOptions);

                  scope.$watch('activeAppIndex',function(value){
                    scope.element.trigger('owl.goTo', scope.activeAppIndex);
                    $(".owl-item").removeClass('selected')
                    $(".owl-item").eq(scope.activeAppIndex).addClass('selected');
                  });

              };

          }
      };
  }

  function owlCarouselItem() {
      return {
          restrict: 'A',
          transclude: false,
          link: function(scope, element) {
            // wait for the last item in the ng-repeat then call init
              if(scope.$last) {
                  scope.initCarousel(element.parent());
              }
          }
      };
  }

})();
