(function() {
    'use strict';

    angular
      .module('studiolo')
      .factory('applicationsFactory', applicationsFactory);

    function applicationsFactory() {
        return {
            apps : [
              {
                app_name : "Camera",
                app_slug : "camera",
                app_icon : "camera",
                app_path : "camera"
              },
              {
                app_name : "3D",
                app_slug : "3Dslash",
                app_icon : "camera",
                app_path : "3D"
              },
              {
                app_name : "Dandy",
                app_slug : "dandy",
                app_icon : "camera",
                app_path : "dandy"
              },
              {
                app_name : "Puppet",
                app_slug : "puppet",
                app_icon : "camera",
                app_path : "puppet"
              },
              {
                app_name : "A propos",
                app_slug : "about",
                app_icon : "camera",
                app_path : "about"
              }
            ]
       };
    }
})();
