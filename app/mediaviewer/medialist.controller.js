(function() {
  angular
   .module('studiolo.mediaViewer')
   .controller('MediaListController', MediaListController);

   MediaListController.$inject = ['$scope','$state','$rootScope','$location','hotkeys','mediaFactory']

   function MediaListController($scope,$state,$rootScope,$location,hotkeys,mediaFactory) {
     //$scope.medias = mediaFactory.loadLibrary();
     $scope.playFile = playFile;

     init();

     function init() {

     }

     function playFile(file){
       mediaFactory.setFile(file);
       $state.go('mediaviewer.player');
     }

     function selectMedia(){
       mediaFactory.setFile($scope.$parent.medias[$scope.$parent.activeMediaIndex]);
       $state.go('mediaviewer.player');
     }

     function previousVideo(){
      if($scope.$parent.activeMediaIndex>0) $scope.$parent.activeMediaIndex--;
     }

     function nextVideo(){
       if($scope.$parent.activeMediaIndex<$scope.$parent.medias.length-1) $scope.$parent.activeMediaIndex++;
     }

     function quitViewer(){
       $state.go('camera');
     }

     hotkeys.bindTo($scope)
     .add ({
       combo : "a",
       description : "Select",
       callback : selectMedia
     })
     .add ({
       combo : "left",
       description : "Select previous Media",
       callback : previousVideo
     })
     .add ({
       combo : "right",
       description : "Select Next Media",
       callback : nextVideo
     })
     .add ({
       combo : "y",
       description : "Quit Viewer",
       callback : quitViewer
     });
   }
})();
