(function() {
  angular
   .module('studiolo.mediaViewer')
   .controller('MediaViewerController', MediaViewerController);

   MediaViewerController.$inject = ['$scope','$state','$rootScope','$location','hotkeys','mediaFactory']

   function MediaViewerController($scope,$state,$rootScope,$location,hotkeys,mediaFactory) {
     $scope.medias = mediaFactory.loadLibrary();
     $scope.playFile = playFile;

     $scope.activeMediaIndex = 0;

     init();

     function init() {

     }

     function playFile(file){
       mediaFactory.setFile(file);
       $state.go('mediaviewer.player');
     }

     function selectMedia(){
       mediaFactory.setFile($scope.medias[$scope.activeMediaIndex]);
       $state.go('mediaviewer.player');
     }

     function previousVideo(){
      if($scope.activeMediaIndex>0) $scope.activeMediaIndex--;
     }

     function nextVideo(){
       if($scope.activeMediaIndex<$scope.medias.length) $scope.activeMediaIndex++;
     }

     function quitViewer(){
       $state.go('camera');
     }

     hotkeys.bindTo($scope)
     .add ({
       combo : "a",
       description : "Select",
       callback : selectMedia
     })
     .add ({
       combo : "left",
       description : "Select previous Media",
       callback : previousVideo
     })
     .add ({
       combo : "right",
       description : "Select Next Media",
       callback : nextVideo
     })
     .add ({
       combo : "y",
       description : "Quit Viewer",
       callback : quitViewer
     });
   }
})();
