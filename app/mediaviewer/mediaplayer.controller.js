(function() {
  angular
   .module('studiolo.mediaViewer')
   .controller('MediaPlayerController', MediaPlayerController);

   MediaPlayerController.$inject = ['$scope','$state','$rootScope','hotkeys','mediaFactory']

   function MediaPlayerController($scope,$state,$rootScope,hotkeys,mediaFactory) {
     $scope.source = mediaFactory.getFile();
     $scope.playerSource = 'medias/'+$scope.source.mode+'/'+$scope.source.filename;

     function playPauseVideo(){
       if(angular.element('.mediaplayer').get(0).paused){
        angular.element('.mediaplayer').get(0).play();
       }else{
        angular.element('.mediaplayer').get(0).pause();
      }
     }

     function restartVideo(){
       angular.element('.mediaplayer').get(0).currentTime = 0;
       angular.element('.mediaplayer').get(0).play();
     }

     function goBack(){
       $state.go('mediaviewer.list');
     }

     hotkeys.bindTo($scope)
     .add ({
       combo : "a",
       description : "Play/Pause Video",
       callback : playPauseVideo
     })
     .add ({
       combo : "b",
       description : "Play from the beginning",
       callback : restartVideo
     })
     .add ({
       combo : "y",
       description : "Go back",
       callback : goBack
     });

   }
})();
