(function() {
    'use strict';

    angular.module('studiolo.camera',['cfp.hotkeys']);
    angular.module('studiolo.keyboard',['cfp.hotkeys']);
    angular.module('studiolo.launcher',['cfp.hotkeys']);
    angular.module('studiolo.mediaViewer',['cfp.hotkeys']);
    angular.module('studiolo.dandy',['cfp.hotkeys','angular-p5','underscorejs']);

    // Library
    angular.module('underscorejs', []);

    angular.module('studiolo',
      [
        'ui.router',
        'cfp.hotkeys',
        'studiolo.launcher',
        'studiolo.camera',
        'studiolo.keyboard',
        'studiolo.mediaViewer',
        'studiolo.dandy'
      ]
    )
    .config(function(hotkeysProvider) {
      hotkeysProvider.useNgRoute = true;
    })
    .run(function($rootScope) {
      $rootScope.referer = "/";
      $rootScope.spawn = require('child_process').spawn;
      $rootScope.fs = require('fs');
    });

})();
