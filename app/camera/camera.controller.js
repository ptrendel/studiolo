(function() {
    'use strict';

    angular
    .module('studiolo.camera')
    .controller('CameraController', CameraController);

    CameraController.$inject = ['$scope','$state','$rootScope','$location','hotkeys','appFactory','fileFactory']

    function CameraController($scope,$state,$rootScope,$location,hotkeys,appFactory,fileFactory) {

      $scope.appModel = appFactory.appModel.camera;
      $rootScope.referer = "/camera";

      $scope.activePath = "/";
      $scope.activeMenuIndex = 0;
      $scope.menu_entries = [];
      $scope.helper = "";
      $scope.process = null;
      $scope.state = 'WAITING';


      $scope.preview = preview;
      $scope.start_video = start_video;
      $scope.stop_video = stop_video;
      $scope.pause_video = pause_video;
      $scope.viewer = viewer;

      $scope.nextMenuItem = nextMenuItem;
      $scope.previousMenuItem = previousMenuItem;
      $scope.executeMenuItem = executeMenuItem;


      init();
      $scope.$watch('activeMenuIndex',updateHelper);


      function init(){
        if (fileFactory.status == 'SAVING') {
          var command = 'MP4Box';
          var args = ['-add', 'temp.h264'];
          args.push('/home/pi/A3/studiolo/medias/videos/'+fileFactory.fname+'.mp4');
          $scope.process = $rootScope.spawn(command,args);

          $scope.process.on('close',function(){
            $scope.state = 'FILE SAVED';
          });
        }

        $scope.menu_entries = getMenuEntries($scope.activePath);
        $scope.helper = $scope.menu_entries[$scope.activeMenuIndex].item_description;

      }

    function preview(){
      var command = "/opt/vc/bin/raspivid";
      var args = ['-p', '200,0,100,100', '-t', '0', '-w', '100', '-h', '100'];
      if($scope.process) $scope.process.kill();
      $scope.process = $rootScope.spawn(command,args);
      $scope.state = 'PREVIEW';
    }

    function viewer(){
      //$location.path('/mediaviewer/list');
      console.log("load viewer");
      $state.go('mediaviewer.list');
    }

    function start_video(){
      if($scope.state =="PAUSE") {
        console.log('On redémare');
        process.kill($scope.process.pid,'SIGUSR1');
      }else{
        console.log('On démare');
        var command = "/opt/vc/bin/raspivid";
        var args = ['-s', '-p', '200,0,640,480', '-t', '0', '-w', '640', '-h', '480', '-o', 'temp.h264'];
        if($scope.process) $scope.process.kill();
        $scope.process = $rootScope.spawn(command,args);

      }
      $scope.state = 'RECORD';

    }

    function pause_video(){
    process.kill($scope.process.pid,'SIGUSR1');
    $scope.state='PAUSE';
    console.log('Hop la Pause');
    $scope.activePath = "/capture/video/"
    $scope.menu_entries = getMenuEntries($scope.activePath);
        $scope.activeMenuIndex = 0;

    }

    function stop_video(){
      console.log('On arrete');
      if($scope.process)
        $scope.process.kill();
      $scope.state = 'WAITING';
      $location.path('/keyboard/');
    }

      function filterPath(path) {
          return function(element) {
              return element.item_path === path;
          }
      }

      function getMenuEntries(path){
        var entries = $scope.appModel.app_items.filter(filterPath(path));
        if(path!="/") entries.unshift ($scope.appModel.app_back_entry);
        return entries;
      }

      function updateHelper(value){
          $scope.helper = $scope.menu_entries[value].item_description;
      }

      function nextMenuItem(){
        if($scope.activeMenuIndex<$scope.menu_entries.length-1) $scope.activeMenuIndex++;
      }

      function previousMenuItem(){
        if($scope.activeMenuIndex>0) $scope.activeMenuIndex--;
      }

      function getBackPath(){
        return $scope.activePath.slice(0,$scope.activePath.substring(0, $scope.activePath.length - 1).lastIndexOf("/")+1);
      }

      function executeAction(target_action) {
        if(target_action) {
          switch (target_action.type) {
            case "link":
              $location.path(target_action.value);
              break;
            case "callback" :
              console.log('execute callback : '+target_action.value);
              $scope[target_action.value]();
              break;
            case "command":
              console.log('execute command');
              var command = target_action.value;
              var args=[];
              $scope.process = $rootScope.spawn(command,args);
              break;
            default:
              console.log('??');
          }
        }
      }
      function executeMenuItem(){
        var target_slug = $scope.menu_entries[$scope.activeMenuIndex].item_slug;
        var target_action = $scope.menu_entries[$scope.activeMenuIndex].item_action;
        if(target_slug=="back") {
          var back_path = getBackPath();
          $scope.activePath = back_path;
          $scope.menu_entries = getMenuEntries(back_path);
          $scope.activeMenuIndex = 0;

        }else{
          executeAction(target_action);

          var target_path  = $scope.activePath + target_slug + "/";
          var target_entries = getMenuEntries(target_path);
          if(target_entries.length>1) {
              $scope.activePath = target_path;
              $scope.menu_entries = target_entries;
              $scope.activeMenuIndex = 0;
          }
        }
      }

      hotkeys.bindTo($scope)
      .add({
        combo: 'down',
        description: 'Next Menu',
        callback: nextMenuItem
      })
      .add ({
        combo : "up",
        description : "Previous Menu",
        callback : previousMenuItem
      })
      .add ({
        combo : "a",
        description : "Execute Command²",
        callback : executeMenuItem
      });

    }
})();
