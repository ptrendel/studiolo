(function() {
    'use strict';

    angular
      .module('studiolo')
      .config(config);

    function config($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.otherwise("/");

      $stateProvider
        .state('launcher', {
          url: "/",
          templateUrl: "app/launcher/launcher.html",
          controller: "LauncherController",
        })
        .state('camera', {
          url: "/camera",
          templateUrl: "app/camera/camera.html",
          controller: "CameraController",
        })
        .state('keyboard', {
          url: "/keyboard",
          templateUrl: "app/keyboard/keyboard.html",
          controller: "KeyboardController",
        })
        .state('mediaviewer', {
          abstract: true,
          url: "/mediaviewer",
          templateUrl: "app/mediaviewer/mediaviewer.html",
          controller: "MediaViewerController"
        })
        .state('mediaviewer.list', {
          url: "/list",
          templateUrl: "app/mediaviewer/media-list.html",
          controller: "MediaListController"
        })
        .state('mediaviewer.player', {
          url: "/player",
          templateUrl: "app/mediaviewer/media-player.html",
          controller: "MediaPlayerController",
        })
        .state('dandy', {
          url: "/dandy",
          templateUrl: "app/dandy/dandy.html",
          controller: "DandyController",
        });
    };


})();
